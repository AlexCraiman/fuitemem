// How strong malloc/free is ? will the heap shrink (brk lowered) ?

#define _XOPEN_SOURCE	500

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include <unistd.h>
#include <malloc.h>

#define	N	1000

static void print (const char *when)
{
  // mallinfo() is an option, but it won't track mmap'ed allocations
  // getrusage() has'nt vmsize
  static int pagesize = 0;
  if (pagesize == 0) pagesize = sysconf(_SC_PAGESIZE);
  FILE *f = fopen("/proc/self/status", "r");
  if (f == NULL) return;
  static int vmpeak = 0;	// Always 0 in W10-WSL ...
  int vmsize = 0;
  while (!feof(f))
  {
    char line[1024];
    if (fgets(line, sizeof(line), f) == NULL) break;
    sscanf(line, "VmSize: %d", &vmsize);
 // sscanf(line, "VmPeak: %d", &vmpeak);
  }
  fclose(f);
  if (vmsize > vmpeak) vmpeak = vmsize;		// W10-WSL
  int const d = vmpeak;
  printf ("%d.%03dko (%s)\n", d/1000, d%1000, when);
}

static int poissonrand(int mean)
{
  typedef long double real_t;
  real_t const el = expl(-mean);
  int   k = 0;
  real_t p = 1.0;
  while (p > el)
  {
    real_t const u = (real_t) random() / RAND_MAX;
    assert ((u >= 0.0) && (u <= 1.0));
    p *= u;
    k++;
  }
  return k;
}

static int random_i (void)
{
  int const i = random() % N;
  assert ((i >= 0) && (i < N));
  return i;
}

static int random_s (void)
{
  // Cf M_TRIM_THRESHOLD, MMAP_THRESHOLD
  int s = poissonrand(100);
  if (random() %  7 == 0) s += poissonrand(5000);
  if (random() % 79 == 0) s += poissonrand(100) * 1024;
  assert (s > 0);
  return s;
}

int main (int argc, char *argv[])
{
  static void *ptrs[N];

#if UTPOISSON
  for (int i = 0; i < 100000; i++)
    printf("%d\n", poissonrand(5000));
  return 0;
#endif

  // Nothing is malloc'ed
  for (int i = 0; i < N; i++) ptrs[i] = NULL;
  print("start");

  // Random malloc's and free's
  int const n = 1000*N;
  for (int k = 0; k < 40*n; k++)
  {
    int const i = random_i();
    if (ptrs[i] == NULL)
    {
      ptrs[i] = malloc(random_s());
      if (ptrs[i] == NULL) print("fail");
    }
    else
    {
      free(ptrs[i]);
      ptrs[i] = NULL;
    }
    if ((k % n) == 0) print("while");
  }

  // Everything is free'd
  for (int i = 0; i < N; i++) if (ptrs[i]) free(ptrs[i]);

  print("exit");

  malloc_stats();
  return 0;
}

