

CC	= gcc
LDFLAGS	= -lm
CFLAGS  = -std=c11 -Wall -O2

all: test

clean:
	-rm -f fuite ut data

test: fuite
	./fuite

ut:
	$(CC) -o ut -g fuite.c -DUTPOISSON $(CFLAGS) $(LDFLAGS)
	./ut | awk -f h.awk > data
	gnuplot -e 'plot "data" w i' -
	

fuite: fuite.c
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS)

